## go 命令行工具
使用 go_command 可以构建自定义命令行工具

### 使用方法
```go
package main

import (
    "fmt"
    "gitee.com/zwx784533/go_command"
    "os"
    "strconv"
)

func main()  {
    // 注册命令
    var routes = map[string]interface{}{
        "test1": test1,
        "test2": test2,
    }
    go_command.RegisterCommand(routes)

    // 命令行参数
    args := os.Args
    if args == nil || len(args) < 2 {
        usage()
        os.Exit(0)
    }
    // 命令
    command := args[1]

    // 剩下的参数传给命令映射的方法
    var params []interface{}
    params = make([]interface{}, len(args)-2)

    for i, param := range args[2:] {
        // 如果参数可以转成整形
        paramInt, notInt := strconv.Atoi(param)
        if notInt != nil {
            params[i] = param
        } else {
            params[i] = paramInt
        }

    }
    // 执行命令
    result, err := go_command.Call(command, params...)
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }
    fmt.Println(result)
    os.Exit(0)

}

func test1(uid int, name string)  {
    fmt.Printf("uid: %d, name: %s", uid, name)
}

func test2(n int)  {
    for i:=0; i<n; i++ {
        fmt.Printf("%d\n", i)
    }
}

// 打印使用方法
func usage() {
    fmt.Println("使用方法:")
    fmt.Println("./console [command] [args...]")
    go_command.Usage()
}
```